# Measurements:

* `dead-channels`

  Tags:
  * (mandatory) `source` -- options: `cal-pulse-scan`
  * (mandatory) `optohybrid` -- wire name (e.g. `GE11-M-01L1`)
  * (mandatory) `vfat`
    
  Fields:
  * `count` -- unsigned

* `seu`

  Tags:
  * (mandatory) `optohybrid` -- wire name (e.g. `GE11-M-01L1`)

  Fields:
  * `corrected-err` -- unsigned
  * `critical-err` -- unsigned
  * `tmr-err` -- unsigned

* `rssi`

  Tags:
  * (mandatory) `optohybrid` -- wire name (e.g. `GE11-M-20L1`)
  * (mandatory) `vtrx`

  Fields:
  * `value` -- float

* `lv-power`

  Tags:
  * (mandatory) `optohybrid` -- wire name (e.g. `GE11-M-01L1`)

  Fields:
  * `value` -- float

* `gbt-status`

  Tags:
  * (mandatory) `fed`
  * (mandatory) `gbt`

  Fields:
  * `always-ready` -- unsigned
  * `ready` -- unsigned

* `vfat-status`

  Tags:
  * (mandatory) `fed`

  Fields:
  * `enabled` -- unsigned
  * `expected` -- unsigned

* `lv-status`

  Tags:
  * (mandatory) `fed`

  Fields:
  * `on` -- unsigned

# Example

```
dead-channels,source=cal-pulse-scan,optohybrid=GE11-M-01L1,vfat=0 count=0u 1661767200000000000
seu,optohybrid=GE11-M-01L1 corrected-err=0u 1668682928387362048
rssi,optohybrid=GE11-M-20L1,vtrx=0 value=394 1668642811801770496
lv-power,optohybrid=GE11-M-01L1 value=30.03 1668797253867093760
gbt-status,fed=1467,gbt=0 always-ready=69u 1668644877239674880
vfat-status,fed=1467 enabled=1623u 1668646742741085696
lv-status,fed=1467 on=72u 1668644877239674880
```
