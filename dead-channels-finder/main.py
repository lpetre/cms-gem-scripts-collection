import argparse
from collections import defaultdict
from dataclasses import dataclass
import datetime
from math import sin, cos

import numpy as np
import pandas as pd
import ROOT

@dataclass
class OptoHybridLocation:
    region: int # +1, -1
    station: int # 1, 2
    number: int # 1..36
    layer: int # 1..8

    def __repr__(self) -> str:
        return f"({self.region}, {self.station}, {self.number}, {self.layer})"

    def get_gem_type(self) -> str:
        if (self.station == 1):
            return "short" if self.number % 2 == 1 else "long"
        elif (self.station == 2):
            return f"m{self.layer}"
        else:
            return "unknown"

    def to_wire_name(self) -> str:
        endcap = "M" if self.region == -1 else "P"
        extension = f"L{self.layer}" if self.station == 1 else f"M{self.layer}"
        return f"GE{self.station}1-{endcap}-{self.number:02}{extension}"

    @classmethod
    def from_wire_name(cls, wire_name: str):
        components = wire_name.split("-")

        region = -1 if components[1] == "M" else 1
        station = int(components[0][2])
        number = int(components[2][:2])
        layer = int(components[2][3])

        return cls(region, station, number, layer)

    def to_display_name(self) -> str:
        endcap = "-" if self.region == -1 else "+"
        extension = f"Ly{self.layer}" if self.station == 1 else f"M{self.layer}"
        return f"GE{endcap}{self.station}/1/{self.number:02} {extension}"

@dataclass
class GEMWheel:
    region: int # +1, -1
    station: int # 1, 2
    layer: int #1..2

    def to_wire_name(self) -> str:
        endcap = "M" if self.region == -1 else "P"
        return f"{endcap}{self.station}L{self.layer}"

    def to_display_name(self) -> str:
        endcap = "-" if self.region == -1 else "+"
        return f"GE{endcap}{self.station}/1 Layer{self.layer}"

def get_vfat_geometry():
    vfat_geometry = defaultdict(lambda: defaultdict(dict))

    vfat_geometry['short'][0]['x'] = np.asarray([-3.83938562003834,-4.14080692494689,-12.4513908518566,-11.5450181214881],dtype=float)
    vfat_geometry['short'][0]['y'] = np.asarray([0,10.206,10.206,0],dtype=float)
    vfat_geometry['short'][1]['x'] = np.asarray([-4.14080692494689,-4.44222822985544,-13.3577635822252,-12.4513908518566],dtype=float)
    vfat_geometry['short'][1]['y'] = np.asarray([10.206,20.412,20.412,10.206],dtype=float)
    vfat_geometry['short'][2]['x'] = np.asarray([-4.44222822985544,-4.79536310569235,-14.4196388259069,-13.3577635822252],dtype=float)
    vfat_geometry['short'][2]['y'] = np.asarray([20.412,32.369,32.369,20.412],dtype=float)
    vfat_geometry['short'][3]['x'] = np.asarray([-4.79536310569235,-5.14849798152926,-15.4815140695887,-14.4196388259069],dtype=float)
    vfat_geometry['short'][3]['y'] = np.asarray([32.369,44.326,44.326,32.369],dtype=float)
    vfat_geometry['short'][4]['x'] = np.asarray([-5.14849798152926,-5.56365370199756,-16.7298857598484,-15.4815140695887],dtype=float)
    vfat_geometry['short'][4]['y'] = np.asarray([44.326,58.383,58.383,44.326],dtype=float)
    vfat_geometry['short'][5]['x'] = np.asarray([-5.56365370199756,-5.97880942246586,-17.9782574501081,-16.7298857598484],dtype=float)
    vfat_geometry['short'][5]['y'] = np.asarray([58.383,72.44,72.44,58.383],dtype=float)
    vfat_geometry['short'][6]['x'] = np.asarray([-5.97880942246586,-6.47069378786385,-19.4573518871341,-17.9782574501081],dtype=float)
    vfat_geometry['short'][6]['y'] = np.asarray([72.4399999999999,89.0949999999999,89.0949999999999,72.4399999999999],dtype=float)
    vfat_geometry['short'][7]['x'] = np.asarray([-6.47069378786385,-6.96257815326184,-20.9364463241602,-19.4573518871341],dtype=float)
    vfat_geometry['short'][7]['y'] = np.asarray([89.0949999999999,105.75,105.75,89.0949999999999],dtype=float)
    vfat_geometry['short'][8]['x'] = np.asarray([-3.83938562003834,-4.14080692494689,4.14080692494689,3.83938562003834],dtype=float)
    vfat_geometry['short'][8]['y'] = np.asarray([0,10.206,10.206,0],dtype=float)
    vfat_geometry['short'][9]['x'] = np.asarray([-4.14080692494689,-4.44222822985544,4.44222822985544,4.14080692494689],dtype=float)
    vfat_geometry['short'][9]['y'] = np.asarray([10.206,20.412,20.412,10.206],dtype=float)
    vfat_geometry['short'][10]['x'] = np.asarray([-4.44222822985544,-4.79536310569235,4.79536310569235,4.44222822985544],dtype=float)
    vfat_geometry['short'][10]['y'] = np.asarray([20.412,32.369,32.369,20.412],dtype=float)
    vfat_geometry['short'][11]['x'] = np.asarray([-4.79536310569235,-5.14849798152926,5.14849798152926,4.79536310569235],dtype=float)
    vfat_geometry['short'][11]['y'] = np.asarray([32.369,44.326,44.326,32.369],dtype=float)
    vfat_geometry['short'][12]['x'] = np.asarray([-5.14849798152926,-5.56365370199756,5.56365370199756,5.14849798152926],dtype=float)
    vfat_geometry['short'][12]['y'] = np.asarray([44.326,58.383,58.383,44.326],dtype=float)
    vfat_geometry['short'][13]['x'] = np.asarray([-5.56365370199756,-5.97880942246586,5.97880942246586,5.56365370199756],dtype=float)
    vfat_geometry['short'][13]['y'] = np.asarray([58.383,72.44,72.44,58.383],dtype=float)
    vfat_geometry['short'][14]['x'] = np.asarray([-5.97880942246586,-6.47069378786385,6.47069378786385,5.97880942246586],dtype=float)
    vfat_geometry['short'][14]['y'] = np.asarray([72.4399999999999,89.0949999999999,89.0949999999999,72.4399999999999],dtype=float)
    vfat_geometry['short'][15]['x'] = np.asarray([-6.47069378786385,-6.96257815326184,6.96257815326184,6.47069378786385],dtype=float)
    vfat_geometry['short'][15]['y'] = np.asarray([89.0949999999999,105.75,105.75,89.0949999999999],dtype=float)
    vfat_geometry['short'][16]['x'] = np.asarray([3.83938562003834,4.14080692494689,12.4513908518566,11.5450181214881],dtype=float)
    vfat_geometry['short'][16]['y'] = np.asarray([0,10.206,10.206,0],dtype=float)
    vfat_geometry['short'][17]['x'] = np.asarray([4.14080692494689,4.44222822985544,13.3577635822252,12.4513908518566],dtype=float)
    vfat_geometry['short'][17]['y'] = np.asarray([10.206,20.412,20.412,10.206],dtype=float)
    vfat_geometry['short'][18]['x'] = np.asarray([4.44222822985544,4.79536310569235,14.4196388259069,13.3577635822252],dtype=float)
    vfat_geometry['short'][18]['y'] = np.asarray([20.412,32.369,32.369,20.412],dtype=float)
    vfat_geometry['short'][19]['x'] = np.asarray([4.79536310569235,5.14849798152926,15.4815140695887,14.4196388259069],dtype=float)
    vfat_geometry['short'][19]['y'] = np.asarray([32.369,44.326,44.326,32.369],dtype=float)
    vfat_geometry['short'][20]['x'] = np.asarray([5.14849798152926,5.56365370199756,16.7298857598484,15.4815140695887],dtype=float)
    vfat_geometry['short'][20]['y'] = np.asarray([44.326,58.383,58.383,44.326],dtype=float)
    vfat_geometry['short'][21]['x'] = np.asarray([5.56365370199756,5.97880942246586,17.9782574501081,16.7298857598484],dtype=float)
    vfat_geometry['short'][21]['y'] = np.asarray([58.383,72.44,72.44,58.383],dtype=float)
    vfat_geometry['short'][22]['x'] = np.asarray([5.97880942246586,6.47069378786385,19.4573518871341,17.9782574501081],dtype=float)
    vfat_geometry['short'][22]['y'] = np.asarray([72.4399999999999,89.0949999999999,89.0949999999999,72.4399999999999],dtype=float)
    vfat_geometry['short'][23]['x'] = np.asarray([6.47069378786385,6.96257815326184,20.9364463241602,19.4573518871341],dtype=float)
    vfat_geometry['short'][23]['y'] = np.asarray([89.0949999999999,105.75,105.75,89.0949999999999],dtype=float)

    vfat_geometry['long'][0]['x'] = np.asarray([-3.83938562003834,-4.17332356777506,-12.5491682745625,-11.5450181214881],dtype=float)
    vfat_geometry['long'][0]['y'] = np.asarray([0,11.307,11.307,0],dtype=float)
    vfat_geometry['long'][1]['x'] = np.asarray([-4.17332356777506,-4.50726151551178,-13.5533184276368,-12.5491682745625],dtype=float)
    vfat_geometry['long'][1]['y'] = np.asarray([11.307,22.614,22.614,11.307],dtype=float)
    vfat_geometry['long'][2]['x'] = np.asarray([-4.50726151551178,-4.90466746092129,-14.7483166110425,-13.5533184276368],dtype=float)
    vfat_geometry['long'][2]['y'] = np.asarray([22.614,36.07,36.07,22.614],dtype=float)
    vfat_geometry['long'][3]['x'] = np.asarray([-4.90466746092129,-5.30207340633079,-15.9433147944483,-14.7483166110425],dtype=float)
    vfat_geometry['long'][3]['y'] = np.asarray([36.07,49.526,49.526,36.07],dtype=float)
    vfat_geometry['long'][4]['x'] = np.asarray([-5.30207340633079,-5.77777328465355,-17.3737425397006,-15.9433147944483],dtype=float)
    vfat_geometry['long'][4]['y'] = np.asarray([49.526,65.633,65.633,49.526],dtype=float)
    vfat_geometry['long'][5]['x'] = np.asarray([-5.77777328465355,-6.2534731629763,-18.804170284953,-17.3737425397006],dtype=float)
    vfat_geometry['long'][5]['y'] = np.asarray([65.633,81.74,81.74,65.633],dtype=float)
    vfat_geometry['long'][6]['x'] = np.asarray([-6.2534731629763,-6.82657530110586,-20.5274862591644,-18.804170284953],dtype=float)
    vfat_geometry['long'][6]['y'] = np.asarray([81.74,101.145,101.145,81.74],dtype=float)
    vfat_geometry['long'][7]['x'] = np.asarray([-6.82657530110586,-7.39967743923543,-22.2508022333757,-20.5274862591644],dtype=float)
    vfat_geometry['long'][7]['y'] = np.asarray([101.145,120.55,120.55,101.145],dtype=float)
    vfat_geometry['long'][8]['x'] = np.asarray([-3.83938562003834,-4.17332356777506,4.17332356777506,3.83938562003834],dtype=float)
    vfat_geometry['long'][8]['y'] = np.asarray([0,11.307,11.307,0],dtype=float)
    vfat_geometry['long'][9]['x'] = np.asarray([-4.17332356777506,-4.50726151551178,4.50726151551178,4.17332356777506],dtype=float)
    vfat_geometry['long'][9]['y'] = np.asarray([11.307,22.614,22.614,11.307],dtype=float)
    vfat_geometry['long'][10]['x'] = np.asarray([-4.50726151551178,-4.90466746092129,4.90466746092129,4.50726151551178],dtype=float)
    vfat_geometry['long'][10]['y'] = np.asarray([22.614,36.07,36.07,22.614],dtype=float)
    vfat_geometry['long'][11]['x'] = np.asarray([-4.90466746092129,-5.30207340633079,5.30207340633079,4.90466746092129],dtype=float)
    vfat_geometry['long'][11]['y'] = np.asarray([36.07,49.526,49.526,36.07],dtype=float)
    vfat_geometry['long'][12]['x'] = np.asarray([-5.30207340633079,-5.77777328465355,5.77777328465355,5.30207340633079],dtype=float)
    vfat_geometry['long'][12]['y'] = np.asarray([49.526,65.633,65.633,49.526],dtype=float)
    vfat_geometry['long'][13]['x'] = np.asarray([-5.77777328465355,-6.2534731629763,6.2534731629763,5.77777328465355],dtype=float)
    vfat_geometry['long'][13]['y'] = np.asarray([65.633,81.74,81.74,65.633],dtype=float)
    vfat_geometry['long'][14]['x'] = np.asarray([-6.2534731629763,-6.82657530110586,6.82657530110586,6.2534731629763],dtype=float)
    vfat_geometry['long'][14]['y'] = np.asarray([81.74,101.145,101.145,81.74],dtype=float)
    vfat_geometry['long'][15]['x'] = np.asarray([-6.82657530110586,-7.39967743923543,7.39967743923543,6.82657530110586],dtype=float)
    vfat_geometry['long'][15]['y'] = np.asarray([101.145,120.55,120.55,101.145],dtype=float)
    vfat_geometry['long'][16]['x'] = np.asarray([3.83938562003834,4.17332356777506,12.5491682745625,11.5450181214881],dtype=float)
    vfat_geometry['long'][16]['y'] = np.asarray([0,11.307,11.307,0],dtype=float)
    vfat_geometry['long'][17]['x'] = np.asarray([4.17332356777506,4.50726151551178,13.5533184276368,12.5491682745625],dtype=float)
    vfat_geometry['long'][17]['y'] = np.asarray([11.307,22.614,22.614,11.307],dtype=float)
    vfat_geometry['long'][18]['x'] = np.asarray([4.50726151551178,4.90466746092129,14.7483166110425,13.5533184276368],dtype=float)
    vfat_geometry['long'][18]['y'] = np.asarray([22.614,36.07,36.07,22.614],dtype=float)
    vfat_geometry['long'][19]['x'] = np.asarray([4.90466746092129,5.30207340633079,15.9433147944483,14.7483166110425],dtype=float)
    vfat_geometry['long'][19]['y'] = np.asarray([36.07,49.526,49.526,36.07],dtype=float)
    vfat_geometry['long'][20]['x'] = np.asarray([5.30207340633079,5.77777328465355,17.3737425397006,15.9433147944483],dtype=float)
    vfat_geometry['long'][20]['y'] = np.asarray([49.526,65.633,65.633,49.526],dtype=float)
    vfat_geometry['long'][21]['x'] = np.asarray([5.77777328465355,6.2534731629763,18.804170284953,17.3737425397006],dtype=float)
    vfat_geometry['long'][21]['y'] = np.asarray([65.633,81.74,81.74,65.633],dtype=float)
    vfat_geometry['long'][22]['x'] = np.asarray([6.2534731629763,6.82657530110586,20.5274862591644,18.804170284953],dtype=float)
    vfat_geometry['long'][22]['y'] = np.asarray([81.74,101.145,101.145,81.74],dtype=float)
    vfat_geometry['long'][23]['x'] = np.asarray([6.82657530110586,7.39967743923543,22.2508022333757,20.5274862591644],dtype=float)
    vfat_geometry['long'][23]['y'] = np.asarray([101.145,120.55,120.55,101.145],dtype=float)

    vfat_geometry['m1'][0]['x'] = np.asarray([-14, -14, -21, -21],dtype=float)
    vfat_geometry['m1'][0]['y'] = np.asarray([0, 30, 30, 0],dtype=float)
    vfat_geometry['m1'][1]['x'] = np.asarray([-14, -14, -21, -21],dtype=float)
    vfat_geometry['m1'][1]['y'] = np.asarray([30, 60, 60, 30],dtype=float)
    vfat_geometry['m1'][2]['x'] = np.asarray([-7, -7, -14, -14],dtype=float)
    vfat_geometry['m1'][2]['y'] = np.asarray([0, 30, 30, 0],dtype=float)
    vfat_geometry['m1'][3]['x'] = np.asarray([-7, -7, -14, -14],dtype=float)
    vfat_geometry['m1'][3]['y'] = np.asarray([30, 60, 60, 30],dtype=float)
    vfat_geometry['m1'][4]['x'] = np.asarray([0, 0, -7, -7],dtype=float)
    vfat_geometry['m1'][4]['y'] = np.asarray([0, 30, 30, 0],dtype=float)
    vfat_geometry['m1'][5]['x'] = np.asarray([0, 0, -7, -7],dtype=float)
    vfat_geometry['m1'][5]['y'] = np.asarray([30, 60, 60, 30],dtype=float)
    vfat_geometry['m1'][6]['x'] = np.asarray([0, 0, 7, 7],dtype=float)
    vfat_geometry['m1'][6]['y'] = np.asarray([0, 30, 30, 0],dtype=float)
    vfat_geometry['m1'][7]['x'] = np.asarray([0, 0, 7, 7],dtype=float)
    vfat_geometry['m1'][7]['y'] = np.asarray([30, 60, 60, 30],dtype=float)
    vfat_geometry['m1'][8]['x'] = np.asarray([7, 7, 14, 14],dtype=float)
    vfat_geometry['m1'][8]['y'] = np.asarray([0, 30, 30, 0],dtype=float)
    vfat_geometry['m1'][9]['x'] = np.asarray([7, 7, 14, 14],dtype=float)
    vfat_geometry['m1'][9]['y'] = np.asarray([30, 60, 60, 30],dtype=float)
    vfat_geometry['m1'][10]['x'] = np.asarray([14, 14, 21, 21],dtype=float)
    vfat_geometry['m1'][10]['y'] = np.asarray([0, 30, 30, 0],dtype=float)
    vfat_geometry['m1'][11]['x'] = np.asarray([14, 14, 21, 21],dtype=float)
    vfat_geometry['m1'][11]['y'] = np.asarray([30, 60, 60, 30],dtype=float)

    vfat_geometry['m2'] = vfat_geometry['m1']
    vfat_geometry['m3'] = vfat_geometry['m1']
    vfat_geometry['m4'] = vfat_geometry['m1']

    return vfat_geometry

def get_vfat_wheel(wheel, content):
    histogram = ROOT.TH2Poly()
    vfat_geometry = get_vfat_geometry()
    
    region = wheel.region

    for ioptohybrid in range(1,37):
        optohybrid_location = OptoHybridLocation(region, 1, ioptohybrid, wheel.layer)

        angle = (ioptohybrid - 1)*10 - 90 # The chamber is originally vertical
        angle = np.radians(angle)

        if (optohybrid_location.to_wire_name() not in content):
            continue

        for ivfat in range(24):
            if (ivfat not in content[optohybrid_location.to_wire_name()]):
                continue

            x = vfat_geometry[optohybrid_location.get_gem_type()][ivfat]['x']
            y = vfat_geometry[optohybrid_location.get_gem_type()][ivfat]['y']

            # In GE1/1, the long SCs have drift board facing the IP while the short SCs have readout borad facing the IP.
            # Applying reflection along y axis if needed
            if (region == 1 and optohybrid_location.get_gem_type() == "long") or (region == -1 and optohybrid_location.get_gem_type() == "short"):
                x = -x

            # GE1/1: Min R ~ 130 cm
            y = y + 130

            in_place_x = x*np.cos(angle) - y*np.sin(angle)
            in_place_y = x*np.sin(angle) + y*np.cos(angle)

            # ROOT doesn't allow us to reverse the x axis; do it manually
            if (region == 1):
                in_place_x = -in_place_x

            selected_bin = histogram.AddBin(4, in_place_x, in_place_y)
            histogram.SetBinContent(selected_bin, content[optohybrid_location.to_wire_name()][ivfat])

            histogram.GetXaxis().SetLimits(-265, 265)
            histogram.GetYaxis().SetLimits(-265, 265)
            histogram.GetXaxis().SetTitle("Global x (cm)")
            histogram.GetYaxis().SetTitle("Global y (cm)")
    
    return histogram

def get_vfat_optohybrid(optohybrid_location, content):
    histogram = ROOT.TH2Poly()
    vfat_geometry = get_vfat_geometry()

    optohybrid_wire_name = optohybrid_location.to_wire_name()
    gem_type = optohybrid_location.get_gem_type()

    for ivfat in range(24):
        if (ivfat not in content[optohybrid_wire_name]):
            continue

        selected_bin = histogram.AddBin(4, vfat_geometry[gem_type][ivfat]['x'], vfat_geometry[gem_type][ivfat]['y'])
        histogram.SetBinContent(selected_bin, content[optohybrid_wire_name][ivfat])

    histogram.GetXaxis().SetLimits(-30, 30)
    histogram.GetYaxis().SetLimits(-5, 125)
    histogram.GetXaxis().SetTitle("Local x (cm)")
    histogram.GetYaxis().SetTitle("Local y (cm)")

    return histogram

def get_dead_channels(filenames, output_prefix):
    content = defaultdict(dict)

    # for iregion, ilayer in [(-1, 1), (-1, 2), (1, 1), (1, 2)]:
    #     wheel = GEMWheel(iregion, 1, ilayer)
    #     for ioptohybrid in range(1,37):
    #         vfats = dict.fromkeys(range(24), 0)
    #         optohybrid_wire_name = OptoHybridLocation(iregion, 1, ioptohybrid, ilayer).to_wire_name()
    #         content[optohybrid_wire_name] = vfats

    # content["GE11-M-01L1"][0] = 1
    # content["GE11-M-06L1"][0] = 6
    # content["GE11-P-01L1"][0] = 1
    # content["GE11-P-06L1"][0] = 6
    # content["GE11-M-01L2"][0] = 1
    # content["GE11-M-06L2"][0] = 6
    # content["GE11-P-01L2"][0] = 1
    # content["GE11-P-06L2"][0] = 6
    # for i in range(12):
    #     content["GE21-P-16M1"][i] = i

    # return content

    df = pd.concat((pd.read_csv(f, sep=";") for f in filenames), ignore_index=True)
    df["alive"] = df["hits"] != 0
    df[df["alive"] == False].to_csv(output_prefix + "-list.dat", sep=";", index=False)
    dfs = df.groupby(["fed", "slot", "oh", "vfat"]).sum()
    dfs["dead"] = 128 - dfs["alive"]

    for i in dfs.iterrows():
        fed = i[0][0]
        slot = i[0][1]
        oh = i[0][2]
        vfatN = i[0][3]
        dead = i[1]["dead"]

        if (dead >= 32):
            print(i[0])

        if (fed == 1467):
            chamber_name = "GE11-M-"
        elif (fed == 1468):
            chamber_name = "GE11-P-"
        elif (fed == 1469):
            chamber_name = "GE21-P-"
        else:
            chamber_name = "UNK-"

        if (fed == 1467) or (fed == 1468):
            pos = 6*(slot//2)+(oh//2)+1
            chamber_name += "{:02}".format(pos)
            chamber_name += "L1" if oh % 2 == 0 else "L2"
        elif (fed == 1469):
            chamber_name += "16"
            chamber_name += "M{:01}".format(oh+1)

        content[chamber_name][vfatN] = dead

    cuts = [32, 64, 128, float("inf")]
    for cut in cuts:
        dfc = dfs.loc[dfs["dead"] < cut, "dead"]
        print(f"== Dead channels/VFAT < {cut} ==")
        print(f"  -> {len(dfc)} VFAT")
        print(f"  -> Average: {dfc.mean():.5f} dead channels/VFAT")

    return content

def draw_labels(region, layer):
    radius = 0.125

    for ioptohybrid in range(1,37):
        chamber_name = OptoHybridLocation(region, 1, ioptohybrid, layer).to_display_name()

        # Clockwise vs counter-clockwise
        if (region == -1):
            angle_deg = (10*(ioptohybrid-1)) % 360
        else:
            angle_deg = (180 - 10*(ioptohybrid-1)) % 360
        angle_rad = np.radians(angle_deg)

        # Adjust the label angle for better readability
        label = ROOT.TText(0.5+radius*cos(angle_rad), 0.5+radius*sin(angle_rad), chamber_name)
        if (angle_deg > 90 and angle_deg < 270):
            label.SetTextAngle(angle_deg+180)
            label.SetTextAlign(31)
        else:
            label.SetTextAngle(angle_deg)
            label.SetTextAlign(11)
        label.SetNDC()
        label.SetTextSize(0.009)
        label.Draw();
        ROOT.SetOwnership(label, False)

def export_influxdb(filename, content, timestamp):
    with open(filename, 'w') as f:
        for optohybrid, vfats in content.items():
            for vfat, dead_count in vfats.items():
                f.write(f"dead-channels,source=cal-pulse-scan,optohybrid={optohybrid},vfat={vfat} count={dead_count}u {int(timestamp)*1000000000}\n")

def main():
    parser = argparse.ArgumentParser(description='Analyzes and plots calibration pulse scans.')
    parser.add_argument('-d', '--date', help='InfluxDB export date (format: %%Y-%%m-%%d %%H:%%M%%z)')
    parser.add_argument('output')
    parser.add_argument('input', nargs='+')
    args = parser.parse_args()

    output_prefix = args.output
    input_filenames = args.input

    content = get_dead_channels(input_filenames, output_prefix)

    if (args.date):
        timestamp = datetime.datetime.strptime(args.date, "%Y-%m-%d %H:%M%z").timestamp()
        export_influxdb(output_prefix + "-influxdb.txt", content, timestamp)

    ROOT.gROOT.SetBatch(ROOT.kTRUE)

    # GE1/1
    canvas1 = ROOT.TCanvas("c1", "c1", 4000, 4000)
    canvas1.Divide(2, 2)

    for ipad, (iregion, ilayer) in [(2, (-1, 1)), (4, (-1, 2)), (1, (1, 1)), (3, (1, 2))]:
        wheel = GEMWheel(iregion, 1, ilayer)

        pad = canvas1.GetPad(ipad)
        pad.cd()
        pad.SetLogz()

        histogram = get_vfat_wheel(wheel, content)
        histogram.SetName(wheel.to_wire_name())
        histogram.SetTitle(wheel.to_display_name())
        histogram.SetMarkerSize(.3) # Reduces the text size when drawing the option "COLZ TEXT"
        histogram.GetXaxis().SetLabelSize(0.03)
        histogram.GetXaxis().SetTickLength(0.005)
        histogram.GetXaxis().SetTitleOffset(1.3)
        histogram.GetYaxis().SetTickLength(0.005)
        histogram.GetYaxis().SetTitleOffset(1.3)
        histogram.GetYaxis().SetLabelSize(0.03)
        histogram.GetZaxis().SetRangeUser(0, 128)
        histogram.SetStats(False)
        histogram.Draw("COLZ TEXT")
        ROOT.SetOwnership(histogram, False)

        pad.Update()

        if (wheel.region == 1):
            histogram.GetXaxis().SetLabelOffset(999)
            histogram.GetXaxis().SetTickLength(0)
            axis = ROOT.TGaxis(pad.GetUxmax(),
                               pad.GetUymin(),
                               pad.GetUxmin(),
                               pad.GetUymin(),
                               histogram.GetXaxis().GetXmin(),
                               histogram.GetXaxis().GetXmax(),
                               histogram.GetXaxis().GetNdivisions(),
                               "-S")
            axis.SetLabelFont(42)
            axis.SetLabelSize(0.03)
            axis.SetLabelOffset(-0.015) # Because our axis is inverted...
            axis.SetTickLength(0.005)
            axis.Draw()
            ROOT.SetOwnership(axis, False)

        draw_labels(wheel.region, wheel.layer)

    canvas1.SaveAs(output_prefix + "-ge11.png")

    # canvas1.Update()
    # input()

    # GE2/1
    canvas2 = ROOT.TCanvas("c2", "c2", 4000, 4000)
    canvas2.Divide(2, 2)

    for ipad, ilayer in [(1, 1), (2, 2), (3, 3), (4, 4)]:
        optohybrid_location = OptoHybridLocation(1, 2, 16, ilayer)

        pad = canvas2.GetPad(ipad)
        pad.cd()
        pad.SetLogz()

        histogram = get_vfat_optohybrid(optohybrid_location, content)
        histogram.SetName(optohybrid_location.to_wire_name())
        histogram.SetTitle(optohybrid_location.to_display_name())
        histogram.GetXaxis().SetTickLength(0.005)
        histogram.GetXaxis().SetTitleOffset(1.3)
        histogram.GetXaxis().SetLabelSize(0.03)
        histogram.GetYaxis().SetTickLength(0.005)
        histogram.GetYaxis().SetTitleOffset(1.3)
        histogram.GetYaxis().SetLabelSize(0.03)
        histogram.GetZaxis().SetRangeUser(0, 128)
        histogram.SetStats(False)
        histogram.Draw("COLZ TEXT")
        ROOT.SetOwnership(histogram, False)

        pad.Update()

    canvas2.SaveAs(output_prefix + "-ge21.png")

    # canvas2.Update()
    # input()

if __name__ == "__main__":
    main()
