from collections import Counter
import glob
import os
import re
import sys

from xlrd import open_workbook, cellname

# super-chamber
rescdir = re.compile(r'^GE11-X-SC([L,S])-(\d{4})$')
rescxls_format = r'^GE11-X-SC{}-{}_Log_File_(\d{{8}}).*.xlsx$'

scname_idx = (0,2) # C1
rescname_format = r'^GE1/1-X-SC{}-{}$'

# chamber
chname_idx  = ((3,2),(3,3)) # C4,D4
rechname_format = r'^GE1/1-X-{}-[A-Z]*-(\d{{4}})$'

# geb
geb_idx = (((4,2),(5,2)),((4,3),(5,3))) # C5,6 D5,6
regeb_format = r'^(?:GE1/1-)?GEB-V3c-([{}])([W,N])-(\d{{1,4}})$'

# oh
oh_idx = ((6,2),(6,3)) # C7, D7
reoh = re.compile(r'^(?:GE1/1-)?OH-V3-(\d{1,4})$')

# Resuts
scsL  = []
chsL  = []
gebsL = []
ohsL  = []

for root, dirs, files in os.walk(sys.argv[1]):
    scdir = os.path.basename(root)
    scdir_match = rescdir.match(scdir)
    if not scdir_match:
        continue

    # Prepare regex
    rescxls = re.compile(rescxls_format.format(scdir_match.group(1),
                                               scdir_match.group(2)))
    rescname = re.compile(rescname_format.format(scdir_match.group(1),
                                                 scdir_match.group(2)))
    rechname = re.compile(rechname_format.format(scdir_match.group(1)))
    regeb = re.compile(regeb_format.format(scdir_match.group(1)))

    # Select the latest assembly file
    scxls = list(filter(None, [rescxls.match(f) for f in files]))
    if len(scxls) == 0:
        print("Warning: {} does not contain any file following the expected super-chamber XLSX naming convention".format(scdir), file=sys.stderr)
        continue
    scxls.sort(key = lambda x: int(x.group(1)), reverse=True)
    scxls = scxls[0].group(0)

    workbook = open_workbook(os.path.join(root, scxls))
    worksheet = workbook.sheet_by_name('Short') # Yes, 'Short'

    # super-chamber
    try:
        sc_name = worksheet.cell(*scname_idx).value
    except IndexError as e:
        sc_name = ""
        print("Caught exception {} while processing super-chamber name from {}".format(e,scxls), file=sys.stderr)

    # chamber
    try:
        ch_names = (worksheet.cell(*chname_idx[0]).value,
                    worksheet.cell(*chname_idx[1]).value)
    except IndexError as e:
        ch_names = ("", "")
        print("Caught exception {} while processing chamber names from {}".format(e,scxls), file=sys.stderr)

    # geb
    try:
        geb_names = ((worksheet.cell(*geb_idx[0][0]).value,
                      worksheet.cell(*geb_idx[0][1]).value),
                     (worksheet.cell(*geb_idx[1][0]).value,
                      worksheet.cell(*geb_idx[1][1]).value))
    except IndexError as e:
        geb_names = (("", ""), ("", ""))
        print("Caught exception {} while processing GEB names from {}".format(e,scxls), file=sys.stderr)

    # oh
    try:
        oh_names = (worksheet.cell(*oh_idx[0]).value,
                    worksheet.cell(*oh_idx[1]).value)
    except IndexError as e:
        oh_names = ("", "")
        print("Caught exception {} while processing OH names from {}".format(e,scxls), file=sys.stderr)

    #print(sc_name, ch_names, geb_names, oh_names)

    # super-chamber
    if not rescname.match(sc_name):
        print("Warning: super-chamber {} not following the input filename {}".format(sc_name, scxls), file=sys.stderr)

    # chamber
    for idx, ch_name in enumerate(ch_names):
        if not rechname.match(ch_name):
            print("Warning: invalid chamber name {} in {}".format(ch_name, scxls), file=sys.stderr)

        # geb
        for geb_name in geb_names[idx]:
            geb_match = regeb.match(geb_name)
            if geb_match and int(geb_match.group(3)) != 0:
                gebsL.append((sc_name, ch_name,
                              "{1}-{0}{2}".format(int(geb_match.group(3)), geb_match.group(1), geb_match.group(2))
                             ))
            else:
                print("Skipping GEB '", geb_name, "' from", scxls, file=sys.stderr)

        # oh
        oh_match = reoh.match(oh_names[idx])
        if oh_match and int(oh_match.group(1)) != 0:
            ohsL.append((sc_name, ch_name, int(oh_match.group(1))))
        else:
            print("Skipping OH '", oh_names[idx], "' from", scxls, file=sys.stderr)

# Remove duplicates
duplicates = [k for k, v in Counter(list(zip(*gebsL))[2]).items() if v != 1]
duplicate_gebs = list(filter(lambda x: x[2] in duplicates, gebsL))
for dup in duplicate_gebs:
    print("Removing duplicate GEB {2} for chamber {1} (SC: {0})".format(*dup), file=sys.stderr)
gebs = set(gebsL) - set(duplicate_gebs)

duplicates = [k for k, v in Counter(list(zip(*ohsL))[2]).items() if v != 1]
duplicate_ohs = list(filter(lambda x: x[2] in duplicates, ohsL))
for dup in duplicate_ohs:
    print("Removing duplicate OH {2} for chamber {1} (SC: {0})".format(*dup), file=sys.stderr)
ohs = set(ohsL) - set(duplicate_ohs)

print("==> Found,", len(gebs), "GEBs :")
for geb in gebs:
    print(geb)
print("==> Found,", len(ohs), "OptoHybrids :")
for oh in ohs:
    print(oh)
