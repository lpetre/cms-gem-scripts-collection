events = [
0x10a83fffffffffffffffffffffffffffffffffffffffffffffffffffc40,
0x10a83fffffffffcd503fffffffffffffffffffffffffffffffffffffc80,
0x28283fffffffffd035d82020203fffffffffffffffffffffffffffffd00,
0x00283fffffffffc195d8202020203fffffffffffffffffffffffffffd40,
0x00003fffffffffda0a18202020203fffffffffffffffffffffffffffd40,
0x00003fffffffffda6260202020203fffffffffffffffffffffffffffd40,
0x00003fffffffffe24a20202020203fffffffffffffffffffffffffffd40,
0x0e403fffffffffedca18202020203fffffffffffffffffffffffffffd40,
0x10a83fffffffffe1b5d8202020203fffffffffffffffffffffffffffd40,
0x10a83fffffffffe1b5d8202020203fffffffffffffffffffffffffffd40,
0x3fefffffffffffd82020202020203fffffffffffffffffffffffffffd40,
0x3fefffffffffffd82020202020203fffffffffffffffffffffffffffd40,
]

def decode(raw):
    address = raw & 0xff
    partition = (raw >> 8) & 0b111
    size = (raw >> 11) & 0b111
    vfat = 8*(address//64) + partition
    if (address != 0xff):
        print("RAW {} - partition {} - address {} - size {} - VFAT {}".format(hex(raw), partition, address, size, vfat))

for data in events:
    print("Event: {}".format(data))
    print("Layer 1")
    for i in range(8):
        decode((data >> (10 + i*14)) & 0x3fff)
    print("Layer 2")
    for i in range(8):
        decode((data >> (122 + i*14)) & 0x3fff)
