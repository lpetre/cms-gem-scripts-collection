import os
import re
import jinja2
import zipfile

def parse_file(filename, field_separator='|', key_separator='.'):
    file_data = []

    with open(filename, 'r') as f:
        flat_keys = f.readline().rstrip().split(field_separator)

        for line in f:
            tree = {}
            flat_data = line.rstrip().split(field_separator)

            for data, flat_key in zip(flat_data, flat_keys):
                t = tree
                prev = None

                for key in flat_key.split(key_separator):
                    if prev is not None:
                        t = t.setdefault(prev, {})
                    prev = key

                t.setdefault(prev, data)

            file_data.append(tree)

    return file_data

def create_zip_file(step):
    with zipfile.ZipFile('./xml/{:02}.zip'.format(step), 'w') as f:
        for dirpath, dirnames, filenames in os.walk('./xml/'):
            for filename in filenames:
                if not re.compile('^{:02}_.*.xml$'.format(step)).match(filename):
                    continue
                f.write(os.path.join(dirpath, filename), arcname=filename)

        for dirpath, dirnames, filenames in os.walk('./templates/{:02}_attachements'.format(step)):
            for filename in filenames:
                f.write(os.path.join(dirpath, filename), arcname=filename)

def main():
    # Load data
    gebs = parse_file('./data/gebs.csv')
    ohs = parse_file('./data/ohs.csv')
    tests = parse_file('./data/tests.csv')

    # Remove local data
    gebs[:]  = (geb  for geb  in gebs  if geb['local'] != 'Y')
    ohs[:]   = (oh   for oh   in ohs   if oh['local'] != 'Y')
    #tests[:] = (test for test in tests if test['local'] != 'Y')

    # Select tests corresponding to components to upload
    active_gebs  = [geb['type'] + geb['part'] + geb['number'] for geb in gebs]
    active_ohs   = [oh['number'] for oh in ohs]

    print(active_gebs)
    print(active_ohs)

    active_tests = []

    for test in tests:
        test_oh = test['oh']['number']
        test_gebw = test['geb']['w']['type'] + test['geb']['w']['part'] + test['geb']['w']['number']
        test_gebn = test['geb']['n']['type'] + test['geb']['n']['part'] + test['geb']['n']['number']

        if ( ((test_oh in active_ohs) or (test_oh == 'na'))
           and ((test_gebw in active_gebs) or (test_gebw == 'naWna'))
           and ((test_gebn in active_gebs) or (test_gebn == 'naNna')) ):
            print(test_oh, test_gebw, test_gebn)
            active_tests.append(test)

    print(len(active_tests))

    # Perform sanity checks

    # Produce XML files
    env = jinja2.Environment(loader=jinja2.FileSystemLoader('./templates'),
                             line_statement_prefix='#')

    for template_filename in env.list_templates(
            filter_func=lambda filename: re.compile("^.*\.xml$").match(filename)):
        template = env.get_template(template_filename)

        with open('./xml/' + template_filename, "w") as f:
            f.write(template.render(gebs = gebs, ohs = ohs, tests = active_tests))

    # Create ZIP files for upload
    for step in range(1, 4):
        create_zip_file(step)

if __name__ == '__main__':
    main()
