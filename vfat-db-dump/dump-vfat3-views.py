import csv
import os

import oracledb

DB_CREDENTIALS = os.environ["VFAT_DB_DUMP_DB"]

db = oracledb.connect(DB_CREDENTIALS, config_dir="/etc")

# Get all VFAT3 views
query = "SELECT VIEW_NAME FROM ALL_VIEWS WHERE OWNER LIKE '%GEM%' AND VIEW_NAME LIKE '%VFAT3%'"

cur = db.cursor()
cur.execute(query)

views = [row[0] for row in cur]

# Restrict to only 1 view?
# views = ["GEM_VFAT3_ARM_DAC_V"]

print(views)

# Fetch view content
for view in views:
    writer = csv.writer(open(f"viewdata-{view}.csv", "w"))
    query = f"SELECT * FROM CMS_GEM_MUON_VIEW.{view}"
    print(query)

    cur = db.cursor()
    cur.execute(query)
    rows = cur.fetchall()
 
    # Save header
    names = [description[0] for description in cur.description]
    print(names)
    writer.writerow(names)

    # Save data
    writer.writerows(rows)
