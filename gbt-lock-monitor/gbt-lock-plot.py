import os, sys
import argparse

import mplhep as hep
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
hep.style.use("CMS")

TOT_GBT0 = 144 + 4
TOT_GBT1 = 144 + 4
TOT_GBT2 = 144
TOT_GBT = TOT_GBT0 + TOT_GBT1 + TOT_GBT2

TOT_VFAT = 144*24 + 4*12

def main():
    ap = argparse.ArgumentParser(add_help=True)
    ap.add_argument('-i', '--input', help='Input GBT lock log filename', required=True)
    ap.add_argument('-o', '--output', help='Output plot filename', required=True)
    options = ap.parse_args()

    dfInput = pd.read_csv(options.input, sep=';')
    dfSum = dfInput.groupby(["time"]).sum().reset_index()

    figure, (axOff, axGBT, axGBTMask, axVFATMask) = plt.subplots(4, 1, figsize=(28, 36), sharex=True)

    time = dfSum["time"]
    time = [np.datetime64(t, 's') for t in time]

    axOff.plot(time, dfSum["off"])
    axOff.set_title('OptoHybrid powered off', y=1.05)
    axOff.set_ylabel('Number OptoHybrid')
    axOff.grid(visible=True)
    axOff.set_ylim(0, 10)

    axGBT.plot(time, TOT_GBT0 - dfSum["ready-gbt0"], label='GBT #0', alpha=0.5)
    axGBT.plot(time, TOT_GBT1 - dfSum["ready-gbt1"], label='GBT #1', alpha=0.5)
    axGBT.plot(time, TOT_GBT2 - dfSum["ready-gbt2"], label='GBT #2', alpha=0.5)
    axGBT.set_title('GBT not ready', y=1.05)
    axGBT.set_ylabel('Number GBT')
    axGBT.legend(frameon=True)
    axGBT.grid(visible=True)
    axGBT.set_ylim(0, 25)

    axGBTMask.plot(time, TOT_GBT - dfSum["expected-gbt"], label='Expected total from GBT', alpha=0.5)
    axGBTMask.plot(time, TOT_GBT0 - dfSum["always-ready-gbt0"], label='GBT #0', alpha=0.5)
    axGBTMask.plot(time, TOT_GBT1 - dfSum["always-ready-gbt1"], label='GBT #1', alpha=0.5)
    axGBTMask.plot(time, TOT_GBT2 - dfSum["always-ready-gbt2"], label='GBT #2', alpha=0.5)
    axGBTMask.set_title('GBT masked', y=1.05)
    axGBTMask.set_ylabel('Number GBT')
    axGBTMask.legend(frameon=True)
    axGBTMask.grid(visible=True)
    axGBTMask.set_ylim(0, 80)

    axVFATMask.plot(time, TOT_VFAT - dfSum["expected-vfat"], label='Expected from GBT', alpha=0.5)
    axVFATMask.plot(time, TOT_VFAT - dfSum["enabled-vfat"], label='Current', alpha=0.5)
    axVFATMask.set_title('VFAT masked', y=1.05)
    axVFATMask.set_ylabel('Number VFAT')
    axVFATMask.legend(frameon=True)
    axVFATMask.grid(visible=True)
    axVFATMask.set_ylim(0, 800)

    axVFATMask.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    axVFATMask.set_xlabel('Date')
    figure.autofmt_xdate()

    #hep.cms.label("Preliminary", data=True, rlabel="GE1/1 @ CERN 904 Lab")

    figure.savefig(options.output)

if __name__=='__main__':
    main()
