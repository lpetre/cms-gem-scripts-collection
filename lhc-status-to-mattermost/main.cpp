#include "DIPVersion.h"
#include "Dip.h"
#include "DipSubscription.h"
#include "DipSubscriptionListener.h"

#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <nlohmann/json.hpp>

#include <chrono>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <regex>
#include <string>
#include <thread>
#include <vector>

struct url_info {
    url_info() {};

    url_info(const std::string& url)
    {
        std::regex re("(?:(https?)://)?([^/:]+)(?::([0-9]+))?(?:/(.+)?)?");
        std::smatch m;

        if (!std::regex_match(url, m, re))
            throw std::runtime_error("Invalid HTTP(S) URL: " + url);

        protocol = "http";
        if (m[1].matched)
            protocol = m[1].str();

        host = m[2].str();

        port = (protocol == "http") ? "80" : "443";
        if (m[3].matched)
            port = m[3].str();

        target = "/";
        if (m[4].matched)
            target += m[4].str();
    }

    std::string protocol;
    std::string host;
    std::string port;
    std::string target;
};

std::string http_post(const std::string& url, const nlohmann::json& data, const std::string& proxy_url = "")
{
    namespace asio = boost::asio;
    using tcp = asio::ip::tcp;
    namespace ssl = boost::asio::ssl;
    namespace beast = boost::beast;
    namespace http = beast::http;

    try {
        // Parse the URLs
        const url_info t_url { url };
        bool use_ssl = (t_url.protocol == "https") ? true : false;

        url_info t_proxy_url;
        bool use_proxy = false;
        if (!proxy_url.empty()) {
            use_proxy = true;
            t_proxy_url = url_info { proxy_url };
            if (t_proxy_url.protocol != "http")
                throw std::runtime_error("Non-HTTP proxy not supported");
        }

        // Boost.Asio I/O objects
        asio::io_context ioc;
        tcp::resolver resolver { ioc };
        tcp::socket socket { ioc };

        // Connect to our host, via HTTP proxy if requested
        if (!use_proxy) {
            const auto results = resolver.resolve(t_url.host, t_url.port);
            asio::connect(socket, results.begin(), results.end());
        } else {
            const auto results = resolver.resolve(t_proxy_url.host, t_proxy_url.port);
            asio::connect(socket, results.begin(), results.end());

            http::request<http::empty_body> request { http::verb::connect, t_url.host + ":" + t_url.port, 11 /* HTTP/1.1 */ };
            request.set(http::field::host, t_url.host + ":" + t_url.port);
            http::write(socket, request);

            // See https://stackoverflow.com/a/49837467/10904212 for more details on parser.skip(true)
            boost::beast::flat_buffer buffer;
            http::response<http::empty_body> response;
            http::parser<false, http::empty_body> parser { response };
            parser.skip(true);
            http::read(socket, buffer, parser);
        }

        // Boost.Asio SSL objects
        ssl::context ctx { ssl::context::tls_client };
        ssl::stream<tcp::socket&> stream { socket, ctx };

        if (use_ssl) {
            ctx.set_default_verify_paths();
            ctx.set_verify_mode(ssl::verify_peer);

            // Set SNI hostname in case virtual hosts would be used by the server
            if (!SSL_set_tlsext_host_name(stream.native_handle(), t_url.host.data())) {
                boost::system::error_code ec { static_cast<int>(::ERR_get_error()), boost::asio::error::get_ssl_category() };
                throw boost::system::system_error { ec };
            }

            stream.handshake(ssl::stream_base::client);
        }

        // Send request
        http::request<http::string_body> request { http::verb::post, t_url.target, 11 /* HTTP/1.1 */ };
        request.set(http::field::host, t_url.host + ":" + t_url.port);
        request.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
        request.set(http::field::content_type, "application/json");
        request.body() = data.dump();
        request.prepare_payload();

        if (use_ssl)
            http::write(stream, request);
        else
            http::write(socket, request);

        // Read reply
        beast::flat_buffer buffer;
        http::response<http::string_body> response;
        if (use_ssl)
            http::read(stream, buffer, response);
        else
            http::read(socket, buffer, response);

        // Gracefully close the socket
        boost::system::error_code ec;

        if (use_ssl) {
            stream.shutdown(ec);
            if (ec == boost::asio::error::eof) {
                // Rationale:
                // http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
                ec.assign(0, ec.category());
            }

            if (ec)
                throw boost::system::system_error { ec };
        } else {
            socket.shutdown(tcp::socket::shutdown_both, ec);
            // not_connected happens sometimes so don't bother reporting it
            if (ec && ec != beast::errc::not_connected)
                throw beast::system_error { ec };
        }

        return response.body();
    } catch (std::exception const& e) {
        throw;
    }
}

std::string postUrl = "";
std::string proxyUrl = "";

struct ValueUpdateDescriptor {
    std::string topic;
    std::string field;
    std::string message;
    std::string lastValue;
};

std::vector<ValueUpdateDescriptor> valueUpdateList = {
    { "dip/acc/LHC/RunControl/MachineMode", "value", "Machine mode updated to " },
    { "dip/acc/LHC/RunControl/BeamMode", "value", "Beam mode updated to " },
    { "dip/acc/LHC/RunControl/RunConfiguration", "ACTIVE_INJECTION_SCHEME", "Injection scheme updated to " },
    { "dip/acc/LHC/RunControl/Page1", "value", "LHC says\n> " },
};

class GenericDataListener : public DipSubscriptionListener {

public:
    /**
     * handle changes to subscribed to publications
     * Simply prints the contents of the received data.
     * @param subscription - the subsciption to the publications thats changed.
     * @param message - object containing publication data
     **/
    void handleMessage(DipSubscription* subscription, DipData& message)
    {

        std::cout << "Received data from " << subscription->getTopicName() << std::endl;
        for (auto& valueUpdate : valueUpdateList) {
            if (valueUpdate.topic != subscription->getTopicName())
                continue;

            const std::string newValue = message.extractString(valueUpdate.field.data());
            if (newValue != valueUpdate.lastValue) {
                nlohmann::json data;
                data["text"] = valueUpdate.message + newValue;
                http_post(postUrl, data, proxyUrl);
                valueUpdate.lastValue = newValue;
            }

            break;
        }
    }

    /**
     * called when a publication subscribed to is available.
     * @param arg0 - the subsctiption who's publication is available.
     **/
    void connected(DipSubscription* arg0)
    {
        std::cout << "Publication source  " << arg0->getTopicName() << " available" << std::endl;
    }

    /**
     * called when a publication subscribed to is unavailable.
     * @param arg0 - the subsctiption who's publication is unavailable.
     * @param arg1 - string providing more information about why the publication is unavailable.
     **/
    void disconnected(DipSubscription* arg0, char* arg1)
    {
        std::cout << "Publication source " << arg0->getTopicName() << " unavailable" << std::endl;
    }

    void handleException(DipSubscription* subscription, DipException& ex)
    {
        std::cout << "Subs " << subscription->getTopicName() << " has error " << ex.what() << std::endl;
    }
};

int main(const int argc, const char** argv)
{
    if (const char* postUrlEnv = std::getenv("LHC_MATTERMOST_POST_URL"); postUrlEnv != nullptr) {
        postUrl = postUrlEnv;
    } else {
        std::cerr << "Please specify the LHC_MATTERMOST_POST_URL environment variable" << std::endl;
        return 1;
    }

    if (const char* proxyUrlEnv = std::getenv("LHC_MATTERMOST_PROXY_URL"); proxyUrlEnv != nullptr)
        proxyUrl = proxyUrlEnv;

    std::cout << " Using " << DipVersion::getDipVersion() << std::endl;

    auto dip = Dip::create();

    GenericDataListener listener {};

    auto subscriptionDeleter = [&dip](DipSubscription* ptr) { dip->destroyDipSubscription(ptr); };

    std::vector<std::unique_ptr<DipSubscription, decltype(subscriptionDeleter)>> subscriptions;
    for (const auto& valueUpdate : valueUpdateList)
        subscriptions.push_back(
            std::unique_ptr<DipSubscription, decltype(subscriptionDeleter)>(
                dip->createDipSubscription(valueUpdate.topic.data(), &listener),
                subscriptionDeleter));

    std::this_thread::sleep_until(std::chrono::time_point<std::chrono::system_clock>::max());

    return 0;
}
