#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

REPOS_URL=${REPOS_URL:-"https://cmsgemos.web.cern.ch/repos"}
BOOTBIN_VERSION=${BOOTBIN_VERSION:-"ctp7-z7-no-xbar"}
LIBS_VERSION=${LIBS_VERSION:-"20210324"}

# Usage
if [ -z $1 ] || [ ! -b $1 ]
then
    echo "Usage: $0 <sd-card-device>"
    exit 1
fi

# Temporary mount point
cleanup() {
    if [ -d ${TMPROOT} ]
    then
        if mountpoint -q ${TMPROOT}/mnt
        then
            umount ${TMPROOT}/mnt
        fi

        rm -r ${TMPROOT}
    fi
}
trap cleanup EXIT INT QUIT ABRT KILL TERM

TMPROOT=$(mktemp -d)

prepare_data() {
    # Download the BOOT.bin file from the repos URL
    echo -e "\nPreparing the 'boot' partition..."

    mkdir ${TMPROOT}/boot && cd ${TMPROOT}/boot

    curl "${REPOS_URL}/firmware/${BOOTBIN_VERSION}/BOOT.bin" -o BOOT.bin

    echo "> 'boot' filesystem tree"
    tree -augp .

    # Persistent partition
    echo -e "\nPreparing the 'persistent' partition..."

    mkdir ${TMPROOT}/persistent && cd ${TMPROOT}/persistent

    # Empty folder to make the Linux image happy
    mkdir lib rpcmodules virtex7

    # System configuration
    for f in ${SCRIPT_DIR}/ctp7-config/*
    do
        install -D -t config $f
    done

    # Additional system libraries
    curl -s "${REPOS_URL}/extras/gem-libs-ctp7-${LIBS_VERSION}.tar.xz" | tar --preserve-permissions --strip-components=1 -xJ -C lib

    # gemdev home
    mkdir gemdev
    for f in ${SCRIPT_DIR}/ctp7-dotfiles/*
    do
        install -D $f gemdev/.${f##*/}
    done
    chown -R 1000:1000 gemdev

    # gempro home
    mkdir -p gempro/{bin,etc,lib,share,var}
    for f in ${SCRIPT_DIR}/ctp7-dotfiles/*
    do
        install -D $f gempro/.${f##*/}
    done
    chown -R 1001:1001 gempro

    echo "> 'persistent' filesystem tree"
    tree -augp .
}

create_partitions() {
    echo "WARNING: This will erase all data from $1!"
    echo "Do you wish to continue?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) break;;
            No ) exit;;
        esac
    done

    echo "Creating the SD-card partitions..."

    sfdisk $1 <<EOF && sleep 1 || exit 1
label: dos
unit: sectors

start=   2048, size=1048576, type=c
start=1050624, size=1048576, type=83
start=2099200,               type=83
EOF

    PARTITIONS=($1?*)

    mkfs.vfat ${PARTITIONS[0]} && sync || exit 1
    mkfs.ext4 -F ${PARTITIONS[1]} && sync || exit 1
    mkfs.ext4 -F ${PARTITIONS[2]} && sync || exit 1

    echo "> The partitions are ${PARTITIONS[@]}"

    mkdir ${TMPROOT}/mnt

    echo "Preparing the 'boot' partition..."
    mount ${PARTITIONS[0]} ${TMPROOT}/mnt
    rsync -av ${TMPROOT}/boot/ ${TMPROOT}/mnt
    umount ${TMPROOT}/mnt

    echo "Preparing the 'persistent' partition..."
    mount ${PARTITIONS[2]} ${TMPROOT}/mnt
    rsync -av ${TMPROOT}/persistent/ ${TMPROOT}/mnt
    umount ${TMPROOT}/mnt
}

prepare_data
create_partitions $1
