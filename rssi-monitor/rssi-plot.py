import os, sys
import argparse

import mplhep as hep
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
hep.style.use("CMS")

def main():
    ap = argparse.ArgumentParser(add_help=True)
    ap.add_argument('-i', '--input', help='Input file with RSSI current values per chamber', required=True)
    ap.add_argument('-o', '--output', help='Output folder', required=True)
    ap.add_argument('-s', '--short', action='store_true', help='Plot monitoring over a long time (hours)')
    ap.add_argument('-l', '--long', action='store_true', help='Plot monitoring over a long time (hours)')
    ap.add_argument('-t', '--time', action='store_true', help='Plot monitoring over a long time (hours)')
    options = ap.parse_args()

    try: os.makedirs(f'{options.output}')
    except FileExistsError: pass

    dfInput = pd.read_csv(options.input, sep=';')

    # Plot data for each OptoHybrid
    for wire_name in dfInput["wire-name"].unique():
        dfOptoHybrid = dfInput[dfInput["wire-name"] == wire_name]

        if options.short:
            figTime, axTime = plt.subplots(1, 1, figsize=(14,12))
            axTime.set_title(wire_name, y=1.05)
            axTime.set_ylim(0, 500)
            axTime.set_ylabel('RSSI current (µA)')
            axTime.grid(visible=True)
            if options.time:
                axTime.set_xlabel('Time (s)')
            else:
                axTime.set_xlabel('Date')
                axTime.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
                figTime.autofmt_xdate()

        if options.long:
            figHistogram, axHistogram = plt.subplots(1, 1, figsize=(14,12))
            axHistogram.set_title(wire_name, y=1.05)
            axHistogram.set_xlim(0, 500)
            axHistogram.set_xlabel('RSSI current (µA)')
            axHistogram.grid(visible=True)
        
        # Plot for all VTRx
        for vtrx in dfOptoHybrid["vtrx"].unique():
            dfVTRX = dfOptoHybrid[dfOptoHybrid["vtrx"]==vtrx]

            # print(wire_name, vtrx)
            # print(dfVTRX, '\n')

            time, rssi = dfVTRX.timestamp, dfVTRX.rssi
            if not options.time:
                time = [np.datetime64(int(t), 's') for t in time]

            if options.short:
                axTime.plot(time, rssi, label=f'RSSI for VTRx #{vtrx}')

            if options.long:
                axHistogram.hist(rssi, label=f'RSSI for VTRx #{vtrx}', alpha=0.5)
            
    
        #hep.cms.label("Preliminary", data=True, rlabel="GE1/1 @ CERN 904 Lab")

        if options.short:
            axTime.legend(frameon=True)
            figTime.savefig(f'{options.output}/{wire_name.replace("/", "-")}.png')
        if options.long:
            axHistogram.legend(frameon=True)
            figHistogram.savefig(f'{options.output}/{wire_name.replace("/", "-")}-hist.png')

if __name__=='__main__':
    main()
