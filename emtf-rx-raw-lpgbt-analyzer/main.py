import sys

import pandas as pd
pd.set_option("display.max_rows", None, "display.max_columns", None)

from reedsolo import RSCodec
rsc = RSCodec(2, c_exp = 5)

def check_header(raw_frame):
    if not ((raw_frame >> 254) & 0b10):
        print("Header not found!")

def deinterleave(raw_data):
    raw_data_string = "{:0256b}".format(raw_data)[::-1]
    raw_data_list = [*raw_data_string]

    # Code 0
    code0_list = ["0"]*145
    fec0_list = ["0"]*10

    code0_list[118::-1] = ( raw_data_list[253:251:-1]
                          + raw_data_list[251:249:-1]
                          + raw_data_list[244:239:-1]
                          + raw_data_list[234:229:-1]
                          + raw_data_list[224:219:-1]
                          + raw_data_list[214:209:-1]
                          + raw_data_list[204:199:-1]
                          + raw_data_list[194:189:-1]
                          + raw_data_list[184:179:-1]
                          + raw_data_list[174:169:-1]
                          + raw_data_list[164:159:-1]
                          + raw_data_list[154:149:-1]
                          + raw_data_list[144:139:-1]
                          + raw_data_list[134:129:-1]
                          + raw_data_list[124:119:-1]
                          + raw_data_list[114:109:-1]
                          + raw_data_list[104:99:-1]
                          + raw_data_list[94:89:-1]
                          + raw_data_list[84:79:-1]
                          + raw_data_list[74:69:-1]
                          + raw_data_list[64:59:-1]
                          + raw_data_list[54:49:-1]
                          + raw_data_list[44:39:-1]
                          + raw_data_list[34:29:-1]
                          + raw_data_list[24:19:-1])

    fec0_list[::-1] = raw_data_list[14:9:-1] + raw_data_list[4::-1];

    # Code 1
    code1_list = ["0"]*145
    fec1_list = ["0"]*10

    code1_list[114::-1] = ( raw_data_list[249:244:-1]
                         + raw_data_list[239:234:-1]
                         + raw_data_list[229:224:-1]
                         + raw_data_list[219:214:-1]
                         + raw_data_list[209:204:-1]
                         + raw_data_list[199:194:-1]
                         + raw_data_list[189:184:-1]
                         + raw_data_list[179:174:-1]
                         + raw_data_list[169:164:-1]
                         + raw_data_list[159:154:-1]
                         + raw_data_list[149:144:-1]
                         + raw_data_list[139:134:-1]
                         + raw_data_list[129:124:-1]
                         + raw_data_list[119:114:-1]
                         + raw_data_list[109:104:-1]
                         + raw_data_list[99:94:-1]
                         + raw_data_list[89:84:-1]
                         + raw_data_list[79:74:-1]
                         + raw_data_list[69:64:-1]
                         + raw_data_list[59:54:-1]
                         + raw_data_list[49:44:-1]
                         + raw_data_list[39:34:-1]
                         + raw_data_list[29:24:-1])

    fec1_list[::-1] = raw_data_list[19:14:-1] + raw_data_list[9:4:-1]

    return pd.Series({0: int(''.join(code0_list)[::-1], 2),
                      1: int(''.join(fec0_list)[::-1], 2),
                      2: int(''.join(code1_list)[::-1], 2),
                      3: int(''.join(fec1_list)[::-1], 2)})

def decode(data):
    code = data[0]
    fec = data[1]
    symbols = [int('{:05b}'.format((code >> i*5) & 0x1f), 2) for i in range(29)] + [int('{:05b}'.format((fec >> i*5) & 0x1f), 2) for i in range(2)]
    decoded_symbols = rsc.decode(symbols)[0]
    msg = 0
    for i in range(len(decoded_symbols)):
        msg = msg | ((int(decoded_symbols[i]) & 0x1f) << i*5)
    return msg

def create_message(data):
    msg0_list = [*"{:0145b}".format(data[0])[::-1]]
    msg1_list = [*"{:0145b}".format(data[1])[::-1]]
    msg_list = ["0"]*234
    msg_list[::-1] = ( msg0_list[118:116:-1]
                     + msg1_list[114::-1]
                     + msg0_list[116::-1])
    return int(''.join(msg_list)[::-1], 2)

def descramble58(scrambled, last_scrambled):
    descrambled = 0
    descrambled = descrambled | ( ~(~(((scrambled >> 39) & 0x7ffff) ^ (scrambled & 0x7ffff)) ^ ((last_scrambled >> 39) & 0x7ffff))) << 39
    descrambled = descrambled | ( ~(~((scrambled & 0x3fffffffff) ^ ((last_scrambled >> 19) & 0x3fffffffff)) ^ (last_scrambled & 0x3fffffffff)));
    return descrambled

def descramble60(scrambled, last_scrambled):
    descrambled = 0
    descrambled = descrambled | ( ~(~(((scrambled >> 58) & 0x3) ^ ((scrambled >> 19) & 0x3 )) ^ (scrambled & 0x3))) << 58
    descrambled = descrambled | ( ~(~(((scrambled >> 39) & 0x7ffff) ^ (scrambled & 0x7ffff)) ^ ((last_scrambled >> 39) & 0x7ffff))) << 39
    descrambled = descrambled | ( ~(~((scrambled & 0x3fffffffff) ^ ((last_scrambled >> 19) & 0x3fffffffff)) ^ (last_scrambled & 0x3fffffffff)));
    return descrambled

def descramble(scrambled, last_scrambled):
    descrambled = 0
    descrambled = descrambled | descramble58(scrambled & 0x3ffffffffffffff,          last_scrambled & 0x3ffffffffffffff)
    descrambled = descrambled | (descramble58((scrambled >> 58) & 0x3ffffffffffffff,  (last_scrambled >> 58) & 0x3ffffffffffffff)) << 58
    descrambled = descrambled | (descramble58((scrambled >> 116) & 0x3ffffffffffffff, (last_scrambled >> 116) & 0x3ffffffffffffff)) << 116
    descrambled = descrambled | (descramble60((scrambled >> 174) & 0xfffffffffffffff, (last_scrambled >> 174) & 0xfffffffffffffff)) << 174
    return descrambled

df = pd.read_csv(sys.argv[1], skiprows = [0, 1], header = None, usecols= [3], converters={3: lambda x: int(x, 16)}, names=["raw_frame"])
df = df.iloc[::4, :]

df["raw_frame"].apply(check_header)
df[["code0", "fec0", "code1", "fec1"]] = df["raw_frame"].apply(deinterleave)
df["msg0"] = df[["code0", "fec0"]].apply(decode, axis=1, raw=True)
df["msg1"] = df[["code1", "fec1"]].apply(decode, axis=1, raw=True)
df["msg"] = df[["msg0", "msg1"]].apply(create_message, axis=1, raw=True)

last_row = df.iloc[0]
for _, row in df.iterrows():
    descrambled = descramble(row["msg"], last_row["msg"])
    mode = (descrambled >> 1) & 0x1
    if (mode == 1):
        print("Mode 1: ", hex((descrambled >> 2) & 0x7f))
    if (mode == 0):
        print("Mode 0")

    last_row = row
